class CreateDiscs < ActiveRecord::Migration
  def change
    create_table :discs do |t|
      t.string :titulo
      t.integer :anio
      t.string :genero

      t.timestamps null: false
    end
  end
end
